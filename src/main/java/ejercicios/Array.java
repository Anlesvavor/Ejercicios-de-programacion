package ejercicios;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 9/03/18
 */
public class Array {

    private List<Object> array = new ArrayList<>();

    public Array(){

    }

    public Array(List<Object> array){
        this.array = array;
    }

    public void setArray(List<Object> array) {
        this.array = array;
    }

    public List<Object> getArray() {
        return array;
    }

    public void addObject(Object o){
        this.array.add(o);
    }


    @Override
    public String toString() {
        String string = "";
        for ( Object o: this.array) {
            string += o.toString();
        }
        return string;
    }
}
