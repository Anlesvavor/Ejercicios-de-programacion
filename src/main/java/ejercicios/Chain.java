package ejercicios;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Chain {

    public Integer countLetter(String string, String c) {
        c = String.valueOf(c.charAt(0));
        Integer count = 0;
        for(Integer i = 0; i < string.length(); i++){
            count += (String.valueOf(string.charAt(i)).equals(c))? 1 : 0;
        }
        return count;
    }
}
