package ejercicios;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 4/03/18
 */
public class DigitalClock {

    private Integer hour;
    private Integer minute;
    private Integer second;

    public DigitalClock(){
        this.hour = 0;
        this.minute = 0;
        this.second = 0;

    }

    public void tick(){

        if (this.second == 59){
            if (this.minute == 59){
                addHour();
            }
            addMinute();
        }
        addSecond();

    }

    public void time(){
        System.out.println(String.format("%d:%d:%d",this.hour, this.minute, this.second));
    }
/*
    public DigitalClock(Integer hour, Integer minute, Integer second){

        this.hour = hour;
        this.minute = minute;
        this.second = second;

    }
    */

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Integer getSecond() {
        return second;
    }

    public void setSecond(Integer second) {
        this.second = second;
    }

    private void addSecond(){ this.second = this.second == 59? 0 : this.second + 1; }

    private void addMinute(){ this.minute = this.minute == 59? 0 : this.minute + 1; }

    private void addHour(){ this.hour = this.hour == 23? 0 : this.hour + 1; }
}
