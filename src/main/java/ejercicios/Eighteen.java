package ejercicios;

import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Eighteen {
    public static void main(String[] args) {

        String string;
        String c;
        Chain ch = new Chain();
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a string");
        string = sc.next();

        System.out.println("Enter a character, taking a the first letter if a string is enter");
        c = sc.next();

        System.out.println(String.format("Character appears %d time(s) in %s",ch.countLetter(string, c), string));

    }
}
