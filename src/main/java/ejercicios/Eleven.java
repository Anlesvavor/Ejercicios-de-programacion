package ejercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Eleven {
    public static void main(String[] args) {

        Print p = new Print();
        Scanner sc = new Scanner(System.in);
        Integer end = sc.nextInt();
        List<Integer> list = new ArrayList<>();

        list = p.mulltiplesOfNSequence(1, end, 3);
        list.stream().forEach((Integer i)->{
            System.out.println(i);
        });
        System.out.println(String.format("Number of elements: %d",list.size()));

    }
}
