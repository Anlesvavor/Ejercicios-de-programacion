package ejercicios;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 6/03/18
 */
public class Factorial {

    public Factorial(){

    }

    public Integer factorial(Integer n){

        //return (n == 0)? 1 : factorial(n - 1);

        if (n == 0){
            return 1;
        } else {
            return n * factorial(n - 1);
        }

    }

}
