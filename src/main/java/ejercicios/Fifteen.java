package ejercicios;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Fifteen {

    public static void main(String[] args) {

        Integer n1, n2;
        Validate v = new Validate();
        Print p = new Print();
        List<Integer> list = new ArrayList<>();
        MyNumber mn = new MyNumber();

        n1 = v.validateNaturalNumber();
        n2 = v.validateNaturalNumber();

        list = p.listSequence(n1, n2);

        list.stream().forEach((Integer i) -> System.out.println(i));
        System.out.println(String.format("There is: %d numbers",list.size()));
        System.out.println(String.format("There is: %d Even numbers",p.evenOrOddSequence(n1, n2, EvenOdd.EVEN).size()));
        System.out.println(String.format("The sum of the Odd numbers is: %d",mn.sum(p.evenOrOddSequence(n1, n2, EvenOdd.ODD))));

    }

}
