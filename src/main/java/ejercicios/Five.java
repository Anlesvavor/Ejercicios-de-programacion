package ejercicios;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Five {
    public static void main(String[] args) {
        Print p = new Print();
        p.printOddSequence(0, 100);
        System.out.println(p.printOddList(0, 100).size());
    }
}
