package ejercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Fourteen {
    public static void main(String[] args) {

        MyNumber mn = new MyNumber();
        List<Double> list = new ArrayList<>();
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter five numbers");

        for(Integer i = 0; i < 5; i++) {
            list.add(i, sc.nextDouble());
        }

        System.out.println(String.format("min: %f, max: %f", mn.min(list), mn.max(list)));

    }
}
