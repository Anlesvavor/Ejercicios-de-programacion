package ejercicios;

import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 8/03/18
 */
public class Menu {

    private Scanner sc;
    private final String menu31 =
            ">>> Menu ------------------------------\n" +
            "1.- Sum \n" +
            "2.- Subtract \n" +
            "3.- Multiplication \n" +
            "4.- Division \n" +
            "5.- Exit\n" +
            "\n" +
            "Option: ";

    private final String menu32 =
            ">>> Menu -----------------------------\n" +
                    "1.- Is prime \n" +
                    "2.- Factorial \n" +
                    "3.- Multiplication table \n" +
                    "4.- Exit\n" +
                    "\n" +
                    "Option: ";

    public void runMenu31(Double n1, Double n2){
        this.sc = new Scanner(System.in);
        Integer option;

        do {
            Double result = 0.00;
            do {
                System.out.println(this.menu31);
                option = this.sc.nextInt();
            } while ( option < 1 || option > 5);

            switch (option) {
                case 1: result = n1 + n2; break;
                case 2: result = n1 - n2; break;
                case 3: result = n1 * n2; break;
                case 4: result = n2 != 0? n1 / n2 : 0 ; break;
                case 5: break;
            }

            System.out.println(result.toString());
        } while (option != 5);

    }

    public void runMenu32(Integer n) {
        this.sc = new Scanner(System.in);
        Prime p = new Prime();
        Factorial f = new Factorial();
        MultiplicationTable t = new MultiplicationTable(n);
        Integer option;

        do {
            String result = "";
            do {
                System.out.println(this.menu32);
                option = this.sc.nextInt();
            } while ( option < 1 || option > 4);

            switch (option) {
                case 1: result = String.valueOf(p.isPrime(n)); break;
                case 2: result = String.valueOf(f.factorial(n)); break;
                case 3: result = t.getTable(); break;

                case 4: break;
            }

            System.out.println(result.toString());
        } while (option != 4);
    }
}
