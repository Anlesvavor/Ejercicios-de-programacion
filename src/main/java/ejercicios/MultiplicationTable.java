package ejercicios;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 8/03/18
 */
public class MultiplicationTable {
    private Integer number;
    private String table;

    public MultiplicationTable(Integer number){
        makeTable(number);
    }

    public String getTable() {
        return table;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void makeTable(Integer number) {
        this.table = "";
        for ( Integer i = 0; i <= 10; i ++ ) {
            this.table += String.format("%d · %d = %d\n", number, i, number * i);
        }
    }
}
