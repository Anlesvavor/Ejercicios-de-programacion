package ejercicios;

import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class MyNumber {

    public Boolean isPositive(Double number){
        return number > 0;
    }

    public Boolean isNegative(Double number){
        return number < 0;
    }

    public Boolean isZero(Double number){
        return number == 0;
    }

    public Sign sign(Double number){
        if (isPositive(number)){
            return Sign.POSITIVE;
        } else if (isNegative(number)){
            return Sign.NEGATIVE;
        } else {
            return Sign.ZERO;
        }
    }

    public Boolean isEven(Integer number) { return number % 2 == 0; }

    public Boolean isOdd(Integer number) { return number % 2 == 1; }

    public EvenOdd isEvenOrOdd(Integer number) { return number % 2 == 0 ? EvenOdd.EVEN :EvenOdd.ODD;  }

    public Integer sum(List<Integer> list) {
        Integer sum = 0;
        for ( Integer i : list ) {
            sum += i;
        }
        return sum;
    }

    public Double max(List<Double> list){
        Double max = list.get(0);
        for ( Double d : list ) {
            max = (d >= max)? d : max;
        }
        return max;
    }

    public Double min(List<Double> list){
        Double min = list.get(0);
        for ( Double d : list ) {
            min = (d <= min)? d : min;
        }
        return min;
    }


}

