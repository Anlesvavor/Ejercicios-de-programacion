package ejercicios;

import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Nine {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Double dbl;
        MyNumber nm = new MyNumber();

        dbl = sc.nextDouble();

        System.out.println(nm.sign(dbl).toString());
    }

}
