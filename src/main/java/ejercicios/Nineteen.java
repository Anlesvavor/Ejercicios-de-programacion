package ejercicios;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 5/03/18
 */
public class Nineteen {
    public static void main(String[] args) {
        DigitalClock d = new DigitalClock();
        Timer t = new Timer();
        Validate v = new Validate();
        Scanner sc = new Scanner(System.in);

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                d.tick();
            }
        };
        t.schedule(task, 10, 1000);

        System.out.println("Enter \"S\" to set the clock, \"N\" to view time");
        while (true) {
            switch (v.validateSN()) {
                case "S":
                    System.out.println("Hour: ");
                    d.setHour(v.validateHour());
                    System.out.println("Minute: ");
                    d.setMinute(v.validateMinuteSecond());
                    System.out.println("Second: ");
                    d.setSecond(v.validateMinuteSecond());
                    break;
                case "N":
                    d.time();
                    break;
            }
        }
    }
}
