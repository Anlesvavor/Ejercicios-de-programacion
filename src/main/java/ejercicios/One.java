package ejercicios;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class One {
    public static void main(String... args){
        Print p = new Print();
        p.printSequence(1, 100);
    }
}
