package ejercicios;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 8/03/18
 */
public class Prime {

    public boolean isPrime(Integer n) {
        for ( Integer i = 2; i < n; i++ ) {
            if ( n % i == 0 ) {
                return false;
            }
        }
        return true;
    }

}
