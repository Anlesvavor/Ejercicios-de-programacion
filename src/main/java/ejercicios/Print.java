package ejercicios;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Print {
    public void printSequence(Integer start, Integer end){
        if (start <= end){
            for(Integer i = start.intValue(); i <= end.intValue(); i++) System.out.println(i);
        } else {
            for(Integer i = start.intValue(); i >= end.intValue(); i--) System.out.println(i);
        }
    }

    public void printEvenSequence(Integer start, Integer end){
        if (start <= end){
            for(Integer i = start.intValue(); i <= end.intValue(); i++){
                if(i % 2 == 0) System.out.println(i);
            }
        } else {
            for(Integer i = start.intValue(); i >= end.intValue(); i--){
                if(i % 2 == 0) System.out.println(i);
            }
        }
    }

    public void printOddSequence(Integer start, Integer end){
        if (start <= end){
            for(Integer i = start.intValue(); i <= end.intValue(); i++){
                if(i % 2 == 1) System.out.println(i);
            }
        } else {
            for(Integer i = start.intValue(); i >= end.intValue(); i--){
                if(i % 2 == 1) System.out.println(i);
            }
        }
    }

    public List<Integer> printOddList(Integer start, Integer end){
        List<Integer> list = new ArrayList<>();
        if (start <= end){
            for(Integer i = start.intValue(); i <= end.intValue(); i++){
                if(i % 2 == 1) list.add(i);
            }
        } else {
            for(Integer i = start.intValue(); i >= end.intValue(); i--){
                if(i % 2 == 1) list.add(i);
            }
        }
        return list;
    }



    public void printSumSequence(Integer start, Integer end){
        Integer sum = 0;
        if (start <= end){
            for(Integer i = start.intValue(); i <= end.intValue(); i++) sum += i;
        } else {
            for(Integer i = start.intValue(); i >= end.intValue(); i--) sum += i;
        }
        System.out.println(sum.toString());
    }

    public Integer sumSequence(Integer start, Integer end){
        Integer sum = 0;
        if (start <= end){
            for(Integer i = start.intValue(); i <= end.intValue(); i++) sum += i;
        } else {
            for(Integer i = start.intValue(); i >= end.intValue(); i--) sum += i;
        }
        return sum;
    }

    public List<Integer> evenOrOddSequence(Integer start, Integer end, EvenOdd eo){
        // La linea de código más hermosa que he hecho hasta la fecha
        Integer modulo = "EVEN".equals(eo.toString()) ? 0 : 1;
        List<Integer> list = new ArrayList<>();

        if (start <= end){
            for(Integer i = start.intValue(); i <= end.intValue(); i++){
                if(i % 2 == modulo) list.add(i);
            }
        } else {
            for(Integer i = start.intValue(); i >= end.intValue(); i--){
                if(i % 2 == modulo) list.add(i);
            }
        }

        return list;
    }

    public List<Integer> mulltiplesOfNSequence(Integer start, Integer end, Integer multiple){
        List<Integer> list = new ArrayList<>();
        if (start <= end){
            for(Integer i = start.intValue(); i <= end.intValue(); i++){
                if(i % multiple.intValue() == 0) list.add(i);
            }
        } else {
            for(Integer i = start.intValue(); i >= end.intValue(); i--){
                if(i % multiple.intValue() == 0) list.add(i);
            }
        }
        return list;
    }

    public List<Integer> mulltiplesOfNSequence(Integer start, Integer end, Integer... multiples){
        List<Integer> list = new ArrayList<>();
            if (start <= end){
                for (Integer j: multiples) {
                    for(Integer i = start.intValue(); i <= end.intValue(); i++){
                        if(i % j.intValue() == 0 && !list.contains(i)) list.add(i);
                    }
                }
            } else {
                for (Integer j : multiples) {
                    for(Integer i = start.intValue(); i >= end.intValue(); i--){
                        if(i % j.intValue() == 0 && !list.contains(i)) list.add(i);
                    }
                }
            }
        return list;
    }

    public List<Integer> listSequence(Integer start, Integer end){

        List<Integer> list = new ArrayList<>();

        if (start <= end){
            for(Integer i = start; i <= end; i++) list.add(i);
        } else {
            for(Integer i = start; i >= end; i--) list.add(i);
        }

        return list;
    }

}
