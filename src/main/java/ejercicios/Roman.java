package ejercicios;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 8/03/18
 */
public class Roman {

    private Integer arabicNumber;
    private String romanNumber;

    public Roman(Integer arabicNumber){
        this.arabicNumber = arabicNumber;
        this.romanNumber = romanNumber();
    }

    public Integer getArabicNumber() {
        return arabicNumber;
    }

    public void setArabicNumber(Integer arabicNumber) {
        this.arabicNumber = arabicNumber;
    }

    public String getRomanNumber() {
        return romanNumber;
    }

    public void setRomanNumber(String romanNumber) {
        this.romanNumber = romanNumber;
    }

    private String romanNumber(){
        Integer tmp = this.arabicNumber;
        Integer th = tmp / 1000;
        Integer h = (tmp - 1000 * th) / 100;
        Integer d = (tmp - 1000 * th - 100 * h) / 10;
        Integer u = (tmp - 1000 * th - 100 * h - 10 * d );
        String roman = "";

        switch (th) {
            case 1: roman += "M";break;
            case 2: roman += "MM";break;
            case 3: roman += "MMM";break;
            case 4: roman += "IV'";break;
            case 5: roman += "V'";break;
            case 6: roman += "VI'";break;
            case 7: roman += "VII'";break;
            case 8: roman += "VIII'";break;
            case 9: roman += "IX'";break;
            default:
        }

        switch (h) {
            case 1: roman += "C";break;
            case 2: roman += "CC";break;
            case 3: roman += "CCC";break;
            case 4: roman += "CD";break;
            case 5: roman += "D";break;
            case 6: roman += "DC";break;
            case 7: roman += "DCC";break;
            case 8: roman += "DCCC";break;
            case 9: roman += "CM";break;
            default:
        }

        switch (d) {
            case 1: roman += "X";break;
            case 2: roman += "XX";break;
            case 3: roman += "XXX";break;
            case 4: roman += "XL";break;
            case 5: roman += "L";break;
            case 6: roman += "LX";break;
            case 7: roman += "LXX";break;
            case 8: roman += "LXXX";break;
            case 9: roman += "XC";break;
            default:
        }

        switch (u) {
            case 1: roman += "I";break;
            case 2: roman += "II";break;
            case 3: roman += "III";break;
            case 4: roman += "IV";break;
            case 5: roman += "V";break;
            case 6: roman += "VI";break;
            case 7: roman += "VII";break;
            case 8: roman += "VIII";break;
            case 9: roman += "IX";break;
            default:
        }

        return roman;

    }

}
