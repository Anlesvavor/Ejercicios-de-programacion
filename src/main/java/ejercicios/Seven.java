package ejercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Seven {

    public static void main(String[] args) {
        List<String> lista = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        String cadena = new String();

        System.out.println("Introduce strings, send \"!\" to stop ");

        while (true){
            cadena = sc.next();
            if(cadena.compareTo("!") == 0) break;
            lista.add(cadena);
        }

        System.out.println(String.format("Number of  introduced phrases: %d",lista.size() ));


    }

}
