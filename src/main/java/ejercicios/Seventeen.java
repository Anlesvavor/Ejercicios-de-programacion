package ejercicios;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Seventeen {

    public static void main(String[] args) {
        Print p = new Print();
        List<Integer> list = new ArrayList<>();
        Validate v = new Validate();
        MyNumber mn = new MyNumber();

        list = p.mulltiplesOfNSequence(v.validateNaturalNumber(), v.validateNaturalNumber(), 2);
        list.stream().forEach((Integer i) -> System.out.println(i));
        System.out.println(String.format("Sum of numbers: %d \nNumber of elements: %d", mn.sum(list), list.size()));

    }

}
