package ejercicios;

import java.util.List;
import java.util.Random;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 8/03/18
 */
public class Simulation {

    private Random r = new Random();

    public String simulateCoin(){

        return r.nextGaussian() > 0.5 ? "Heads" : "Tails" ;
    }

    public Integer simulateDice(){
        Double ran = Math.random() * 6;
        if (ran < 1){
            return 1;
        } else if (ran < 2) {
            return 2;
        } else if (ran < 3) {
            return 3;
        } else if (ran < 4) {
            return 4;
        } else if (ran < 5) {
            return 5;
        } else  {
            return 6;
        }
    }

    public Integer simulateTwoDices() {
        return simulateDice() + simulateDice();
    }

}
