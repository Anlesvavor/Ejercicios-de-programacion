package ejercicios;

import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Six {
    public static void main(String[] args) {
        Print p = new Print();
        Scanner sc = new Scanner(System.in);
        Integer fin;

        System.out.println("number: ");
        fin = sc.nextInt();
        if(fin > 0){
            p.printSequence(1, fin);
        } else {
            System.out.println(String.format("%d It's not a natural number", fin));
        }

    }
}
