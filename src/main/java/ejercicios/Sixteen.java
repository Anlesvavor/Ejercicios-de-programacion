package ejercicios;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Sixteen {

    public static void main(String[] args) {

        Print p = new Print();

        for (Integer i = 0; i < 10; i++) p.printSequence(1, 10);
    }
}
