package ejercicios;

import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 7/03/18
 */
public class Table {

    public void tableFromList(List<Integer> list){

        // El metodo más fumado que he hecho.

        Integer len;
        Double rank = Math.floor(Math.sqrt(Double.parseDouble(String.valueOf(list.size()))));
        String space = "";
        String row = "";
        Integer count = 1;
        Integer max = list.get(0);

        for ( Integer x: list ) {
            max = Math.max(max, x);
        }

        len = max.toString().length();


        for ( Integer x : list ) {
            for ( Integer i = 0; i < len - x.toString().length() + 1; i++ ) space += " ";
                row += String.format("%d%s", x, space);
            if (count % rank == 0) {
                System.out.println(row);
                row = "";
            }
            count++;
            space = "";
        }
        System.out.println(row);
        row = "";
    }
}
