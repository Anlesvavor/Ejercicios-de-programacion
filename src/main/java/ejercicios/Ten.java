package ejercicios;

import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Ten {

    public static void main(String[] args) {

        Integer number;
        Scanner sc = new Scanner(System.in);
        MyNumber mn = new MyNumber();

        number = sc.nextInt();
        System.out.println(mn.isEvenOrOdd(number).toString());
    }
}

