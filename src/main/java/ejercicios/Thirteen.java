package ejercicios;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Thirteen {
    public static void main(String[] args) {

  //      List<Integer> listMult2 = new ArrayList<>();
  //      List<Integer> listMult3 = new ArrayList<>();
        Print p = new Print();
        List<Integer> list = new ArrayList<>();

        list = p.mulltiplesOfNSequence(1, 100,2, 3);
        list.stream().forEach((Integer i) -> System.out.println(i));
        System.out.println(String.format("Number of multiple of 2 or 3: %d", list.size()));


    }
}
