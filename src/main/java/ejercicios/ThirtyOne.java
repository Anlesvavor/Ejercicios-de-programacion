package ejercicios;

import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 8/03/18
 */
public class ThirtyOne {
    public static void main(String[] args) {
        Double n1, n2;
        Scanner sc = new Scanner(System.in);
        Menu m = new Menu();

        System.out.println("Enter two numbers: ");
        n1 = sc.nextDouble();
        n2 = sc.nextDouble();

        m.runMenu31(n1, n2);

    }
}
