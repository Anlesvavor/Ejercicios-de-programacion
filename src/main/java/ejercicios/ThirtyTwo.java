package ejercicios;

import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 9/03/18
 */
public class ThirtyTwo{
        public static void main(String[] args) {
            Integer n;
            Scanner sc = new Scanner(System.in);
            Menu m = new Menu();

            System.out.println("Enter a number: ");
            n = sc.nextInt();

            m.runMenu32(n);

        }
}
