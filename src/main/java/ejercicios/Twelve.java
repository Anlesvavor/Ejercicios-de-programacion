package ejercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Twelve {
    public static void main(String[] args) {

        MyNumber nm = new MyNumber();
        List<Integer> listOdd = new ArrayList<>();
        List<Integer> listEven = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        Print p = new Print();

        listEven = p.evenOrOddSequence(1, 100, EvenOdd.EVEN);
        listOdd = p.evenOrOddSequence(1, 100, EvenOdd.ODD);

        listEven.stream().forEach((Integer i)->{
            System.out.println(i);
        });
        System.out.println(String.format("Sum of even elements: %d", nm.sum(listEven)));

        listOdd.stream().forEach((Integer i)->{
            System.out.println(i);
        });
        System.out.println(String.format("Sum of odd elements: %d", nm.sum(listOdd)));
    }
}
