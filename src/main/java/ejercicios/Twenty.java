package ejercicios;

import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 6/03/18
 */
public class Twenty {
    public static void main(String[] args) {
        Factorial f = new Factorial();
        Scanner sc = new Scanner(System.in);
        Integer n;

        System.out.println("Enter a number");
        n = sc.nextInt();
        System.out.println(String.format("%d! = %d", n, f.factorial(n)));
    }
}
