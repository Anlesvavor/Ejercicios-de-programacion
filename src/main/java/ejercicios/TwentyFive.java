package ejercicios;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 8/03/18
 */
public class TwentyFive {
    public static void main(String[] args) {
        Validate v = new Validate();

        System.out.println("Enter the number to convert: ");
        Roman r = new Roman(v.validateNaturalNumber());
        System.out.println(String.format("%d roman number is: %s", r.getArabicNumber(), r.getRomanNumber()));
        System.out.println("Note: the character \" ' \" denotes a overline");
    }
}
