package ejercicios;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 8/03/18
 */
public class TwentyFour {
    public static void main(String[] args) {
        Validate v = new Validate();
        Prime p = new Prime();
        Integer n = new Integer(v.validateNaturalNumber());
        System.out.println(String.format("%d is %s prime number", n, p.isPrime(n)? "a" : "not"));
    }
}
