package ejercicios;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 8/03/18
 */
public class TwentyNine {
    public static void main(String[] args) {
        Simulation s = new Simulation();
        Integer roll;
        Integer count = 0;
        for ( Integer i = 0; i < 100; i++ ) {
            roll = s.simulateTwoDices();
            System.out.println(roll);
            if (roll == 10) {
                count++;
            }
        }
        System.out.println(String.format("The roll of dice results in 10, %d times",count));
    }
}
