package ejercicios;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 7/03/18
 */
public class TwentyOne {
    public static void main(String[] args) {
        MyNumber mn = new MyNumber();
        List<Integer> listEven = new ArrayList<>();
        List<Integer> listOdd = new ArrayList<>();

        for ( Integer i = 1; i <= 1000; i++ ) {
            switch ( i % 2 ){
                case 0:
                    listEven.add(i);
                    break;
                case 1:
                    listOdd.add(i);
                    break;
            }
        }
        System.out.println(String.format("Sum of even numbers: %d",mn.sum(listEven)));
        System.out.println(String.format("Sum of odd numbers: %d",mn.sum(listOdd)));
    }
}
