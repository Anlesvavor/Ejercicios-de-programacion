package ejercicios;

import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 8/03/18
 */
public class TwentySeven {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number: ");
        Integer numeber = new Integer(sc.nextInt());
        MultiplicationTable mt = new MultiplicationTable(numeber);
        System.out.println(String.format("%s", mt.getTable()));

    }
}
