package ejercicios;

import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 7/03/18
 */
public class TwentyTwo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String string;
        String col = "";

        System.out.println("Enter a phrase");
        string = sc.next();

        for (Integer i = 0; i < 5; i++) {
            col += " ";
            System.out.println(String.format("%s%s", col, string));
        }
    }
}
