package ejercicios;

import com.sun.xml.internal.ws.api.model.ExceptionType;

import java.util.Scanner;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 3/03/18
 */
public class Validate {

    public String validateSN(){
        Scanner sc = new Scanner(System.in);
        String validate;

        do{
            System.out.println("Type S or N");
            validate = sc.next();
        }while (!validate.matches("^S|N$"));

        return validate;
    }

    public Integer validateNaturalNumber(){
        Scanner sc = new Scanner(System.in);
        Integer validate = 0;

        do{
            System.out.println("Enter a natural number");
            validate = sc.nextInt();
        }while (validate <= 0);

        return validate;
    }

    public Integer validateMinuteSecond(){
        Scanner sc = new Scanner(System.in);
        Integer validate = 0;

        do{
            System.out.println("Enter a natural number");
            validate = sc.nextInt();
        }while (validate < 0 || validate > 59);

        return validate;
    }

    public Integer validateHour(){
        Scanner sc = new Scanner(System.in);
        Integer validate = 0;

        do{
            System.out.println("Enter a natural number");
            validate = sc.nextInt();
        }while (validate < 0 || validate > 23);

        return validate;
    }

}
